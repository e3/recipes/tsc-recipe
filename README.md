# tsc conda recipe

Home: "https://github.com/icshwi/tsc"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS tsc module
